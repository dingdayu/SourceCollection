/*
*
*	Time ：2014年1月7日20:48:28
*	类型 ：javascript
*	项目 ：javascript点击运行html
*	来源 : 网络
*	作者 ：jb51.net
*	功能 : javascript点击运行html
*
*/

function runCode()
{
	if(1 == arguments.length)
		try{event = arguments[0];}catch(e){}
	var code=(event.target || event.srcElement).parentNode.childNodes[0].value;//即要运行的代码。
	var newwin=window.open('','','');  //打开一个窗口并赋给变量newwin。
	newwin.opener = null // 防止代码对论谈页面修改
	newwin.document.write(code);  //向这个打开的窗口中写入代码code，这样就实现了运行代码功能。
	newwin.document.close();
}
//运行代码