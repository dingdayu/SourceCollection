/*
Navicat MySQL Data Transfer

Source Server         : 本机
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : linshi

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2014-05-27 23:50:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `username` char(32) NOT NULL COMMENT '用户登陆名',
  `password` char(60) NOT NULL COMMENT '用户密码',
  `email` char(32) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'dingdayu', '123456', '614422099@qq.com');
INSERT INTO `user` VALUES ('2', 'admin', '123456', '614422099@qq.com');
INSERT INTO `user` VALUES ('3', 'ceshi2', '123456', '665465454@qq.com');
