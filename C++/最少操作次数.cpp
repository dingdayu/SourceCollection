/*
*
*	Time ：2014年1月7日21:32:19
*	类型 ：C
*	项目 ：求最少操作次数
*	来源 : 网络
*	作者 ：dingdayu
*	功能 ：给定两个字符串，仅由小写字母组成，它们包含了相同字符。
*	求把第一个字符串变成第二个字符串的最小操作次数，且每次操作只能对第一个字符串中的某个字符移动到此字符串中的开头。
*	例如给定两个字符串“abcd" "bcad" ，输出：2，因为需要操作2次才能把"abcd"变成“bcad" ，方法是：abcd->cabd->bcad。
*
*/

#include <stdio.h>
#include <iostream>
#include <string>
using namespace std;
class Test {
public:
    static int getNumber (string   a,string   b)
    {
        int len = a.length();
        int prevA = len-1;
        int currA = len-1;
        int prevB = len-1;
        int currB = len-1;

        int result = 0;
        while (currA >= 0)
        {
            if (a[currA] == b[currB])
            {
                result += prevA-currA-(prevB-currB);
                prevA = currA;
                prevB = currB;
               currA--;
               currB--;
            }
           else
           {
              currA--;
            }
        }

        result += prevA;

        return result;
    }
};
//start 提示：自动阅卷起始唯一标识，请勿删除或增加。
int main()
{   
    cout<<Test::getNumber("abcd","bcad")<<endl;   
} 
//end //提示：自动阅卷结束唯一标识，请勿删除或增加。