@echo off

:menu

echo 功能菜单：
echo 1.修改为上网IP
echo 2.修改为听课IP
echo 3.修改为自动获取
echo 4.自动退出

set /p choice=请输入命令，按回车确认：

if "%choice%"=="1" goto ip1
if "%choice%"=="2" goto ip2

if "%choice%"=="3" goto auto

if "%choice%"=="4" goto end

goto main

:ip1

echo .
echo IP设置开始....
echo .
echo 正在设置为上网IP

cmd /c netsh interface ip set address name="本地连接" source=static addr=192.168.81.99 mask=255.255.255.0 gateway=192.168.81.254 gwmetric=1

echo 4.自动退出
set /p choice2=是否设置DNS（Y:1/N:2）:

if "%choice2%"=="1" goto setdns
if "%choice2%"=="2" goto menu

if "%choice2%"=="4" goto end

:setdns

echo 正在设置主DNS……

cmd /c netsh interface ip set dns name="本地连接" source=static addr=8.8.8.8 register=PRIMARY 

echo 正在设置副DNS……

netsh interface ip add dns name="本地连接" addr=8.8.4.4

echo 设置完成

pause
exit 

:ip2

echo IP设置开始....
echo .
echo 正在设置为听课IP

cmd /c netsh interface ip set address name="本地连接" source=static addr=192.168.90.98 mask=255.255.255.0 gateway=192.168.90.1 gwmetric=1

echo 4.自动退出
set /p choice2=是否设置DNS（Y:1/N:2）:

if "%choice2%"=="1" goto setdns
if "%choice2%"=="2" goto menu

if "%choice%"=="4" goto end

echo 设置完成

pause
exit 


:auto

echo IP设置开始....
echo 
echo 自动获取IP地址....

netsh interface ip set address name = "本地连接" source = dhcp

echo 自动获取DNS服务器....

netsh interface ip set dns name = "本地连接" source = dhcp 

@rem 设置自动获取IP
echo 设置完成

pause
exit 

:end  